{{ $jqueryjs := resources.Get "js/vendor/jquery-1.10.2.min.js" }}
{{ $bootstrapjs := resources.Get "js/vendor/bootstrap.min.js" }}
{{ $mixitupjs := resources.Get "js/vendor/mixitup.js" }}
{{ $waypointsjs := resources.Get "js/vendor/waypoints.min.js" }}

{{ $js := slice $jqueryjs $bootstrapjs $mixitupjs $waypointsjs | resources.Concat "js/bundle.js" }}

// Load lazy images if it is needed
{{ $lazylist := findRE "class=\".*?\\blazyload\\b.*?\"|class=lazyload\b" .Content }}
{{ if ge (len $lazylist) 1 }}
  {{ $lazyjs := resources.Get "js/vendor/lazysizes.min.js" }}
  {{ $currentbundle := $js }}
  {{ $js := slice $lazyjs $currentbundle | resources.Concat "js/bundle.js" }}
{{ end }}

// Load owl carousel if needed
{{ $carousel := findRE "class=\".*?\\bowl-carousel\\b.*?\"" .Content }}
{{ if ge (len $carousel) 1 }}
  {{ $currentbundle := $js }}
  {{ $carouseljs := resources.Get "js/vendor/owl.carousel.min.js" }}
  {{ $js := slice $carouseljs $currentbundle | resources.Concat "js/bundle.js" }}
{{ end }}
