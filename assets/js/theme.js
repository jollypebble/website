{{ $mainjs := resources.Get "js/theme/main.js" }}
{{ $debugjs := resources.Get "js/theme/debug.js" }}

{{ $js := slice $mainjs $debugjs | resources.Concat "js/bundle.js" }}
