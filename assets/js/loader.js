// Hugo Pipes JS Asset Template Bundler
// Get our vendor JS
{{ $vendorjs := resources.Get "js/vendor.js" }}

// Get our theme JS
{{ $themejs := resources.Get "js/theme.js" }}

{{ $js := slice $vendorjs $themejs | resources.Concat "js/bundle.js" }}
